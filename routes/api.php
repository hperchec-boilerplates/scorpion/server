<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * -------------
 * PUBLIC ROUTES
 * -------------
 */

/**
 * Users
 */

/**
 * [POST] /api/login
 * 
 * Tested in: - tests/Feature/AuthTest::test_login
 */
Route::post('/login', [AuthController::class, 'login'])
     ->name('auth.login');

/**
 * [POST] /api/forgot-password
 * 
 * ! NOT TESTED
 */
Route::post('/forgot-password', [AuthController::class, 'sendResetPasswordMail'])
     ->name('password.email');

/**
 * [POST] /api/reset-password
 * 
 * ! NOT TESTED
 */
Route::post('/reset-password', [AuthController::class, 'resetPassword'])
     ->name('password.reset');

/**
 * [POST] /api/users/create
 * 
 * Tested in: - tests/Feature/UserTest::test_create_user
 */
Route::post('/users/create', [UserController::class, 'create'])
     ->name('users.create');

/**
 * [POST] /api/users/check-email
 * 
 * Tested in: - tests/Feature/UserTest::test_check_email
 */
Route::post('/users/check-email', [UserController::class, 'checkEmail'])
     ->middleware(['throttle:30,1']) // Limited to 30 requests by minute
     ->name('users.checkEmail');

/**
 * ---------------------
 * PRIVATE (AUTH) ROUTES
 * ---------------------
 */

Route::group( [ 'middleware' => [ 'auth:api' ] ] , function () use ($router) {
    
    /**
     * Auth
     */

    /**
     * [GET] /api/me
     * 
     * Tested in: - tests/Feature/AuthTest::test_get_user_data
     */
    $router->get('/me', [AuthController::class, 'getCurrentUser'])
           ->name('auth.me');
    
    /**
     * [PATCH] /api/me/password
     * 
     * Tested in: - tests/Feature/AuthTest::test_update_user_password
     */
    $router->patch('/me/password', [AuthController::class, 'updatePassword']) // Specific for password
           ->name('password.update');
    
    /**
     * [POST] /api/logout
     * 
     * Tested in: - tests/Feature/AuthTest::test_logout
     */
    $router->post('/logout', [AuthController::class, 'logout'])
           ->name('auth.logout');

    /**
     * Users
     */

    /**
     * [PATCH] /api/users/{userId}
     * 
     * Tested in: - tests/Feature/UserTest::test_update_user
     */
    $router->patch('/users/{user}', [UserController::class, 'update'])
           ->where('user', '[0-9]+')
           ->name('users.update');

    /**
     * [POST] /api/users/{userId}/thumbnail
     * !!! Routes using content-type:multipart-formdata must be 'POST' type, not 'PATCH' (known issue... https://github.com/laravel/framework/issues/13457)
     * 
     * Tested in: - tests/Feature/UserTest::test_update_user_thumbnail
     */
    $router->post('/users/{user}/thumbnail', [UserController::class, 'updateThumbnail']) // Specific file upload (formData)
           ->where('user', '[0-9]+')
           ->name('users.update.thumbnail');

    /**
     * [GET] /api/users/{userId}/thumbnail
     * 
     * Tested in: - tests/Feature/UserTest::test_get_user_thumbnail
     */
    $router->get('/users/{user}/thumbnail', [UserController::class, 'getUserThumbnail'])
           ->where('user', '[0-9]+')
           ->name('user.thumbnail');

    /**
     * [POST] /api/users/{userId}/email/verify/{hash}
     * 
     * ! NOT TESTED
     */
    $router->post('/users/{id}/email/verify/{hash}', [UserController::class, 'verifyEmail']) // Specific for email verif
           ->middleware(['signed'])
           ->where('id', '[0-9]+')
           ->name('verification.verify');

    /**
     * [POST] /api/users/{userId}/email/resend-verification-link
     * 
     * ! NOT TESTED
     */
    $router->post('/users/{user}/email/resend-verification-link', [UserController::class, 'resendEmailVerificationLink']) // Resend email verification link.
           ->middleware(['throttle:6,1']) // Limited to 6 requests by minute
           ->where('user', '[0-9]+')
           ->name('verification.send');

});
