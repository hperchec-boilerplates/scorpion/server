<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use Laravel\Passport\Passport;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

use App\Models\User;

class UserTest extends TestCase
{
    /**
     * Set up method
     */
    public function setUp() :void
    {
        // Always call parent set up
        parent::setUp();
        // Do logic here
    }

    /**
     * Test create user
     * 
     * @author Hervé Perchec
     * @group User
     * @covers App\Http\Controllers\UserController::create
     * @covers App\Http\Requests\User\CreateRequest
     * 
     * @return User
     */
    public function test_create_user ()
    {
        // Valid request
        $response = $this->post('/api/users/create', [
            'firstname' => 'string',
            'lastname' => 'string',
            'password' => 'Azerty123*',
            'password_confirmation' => 'Azerty123*',
            'email' => 'herve.perchec@gmail.com'
        ]);
        $response->assertStatus(201);
        // Get created user ID
        $userId = json_decode($response->content())->id;
        // Return created user
        return User::findOrFail($userId);
    }

    /**
     * Test update user
     * 
     * @author Hervé Perchec
     * @group User
     * @covers App\Http\Controllers\UserController::update
     * @covers App\Policies\UserPolicy::update
     * @covers App\Http\Requests\User\UpdateRequest
     * 
     * @depends test_create_user
     * 
     * @param User $user - The previously created user
     * @return void
     */
    public function test_update_user (User $user)
    {
        // Authenticate as user
        Passport::actingAs($user);
        // Valid request
        $response = $this->patch('/api/users/' . $user->id, [
            'firstname' => 'updated firstname',
            'lastname' => 'updated lastname',
            'password' => 'Azerty123*'
        ]);
        $response->assertStatus(200);
    }

    /**
     * Test update user thumbnail
     * 
     * @author Hervé Perchec
     * @group User
     * @covers App\Http\Controllers\UserController::updateThumbnail
     * @covers App\Policies\UserPolicy::updateThumbnail
     * @covers App\Http\Requests\User\UpdateThumbnailRequest
     * 
     * @depends test_create_user
     * 
     * @param User $user - The previously created user
     * @return User
     */
    public function test_update_user_thumbnail (User $user)
    {
        // Authenticate as user
        Passport::actingAs($user);
        // Create fake image
        $fakeImageFileName = $user->generateThumbnailFileName('jpg');
        $fakeImage = UploadedFile::fake()->image($fakeImageFileName, 200, 200)->size(100);
        // Valid request
        $response = $this->post('/api/users/' . $user->id . '/thumbnail', [
            'password' => 'Azerty123*',
            'thumbnail' => $fakeImage
        ]);
        $response->assertStatus(200);
        // Check if file exists
        Storage::disk('thumbnails')->assertExists($user->thumbnail);
        // Return user
        return $user;
    }

    /**
     * Test get user thumbnail
     * 
     * @author Hervé Perchec
     * @group User
     * @covers App\Http\Controllers\UserController::getUserThumbnail()
     * @covers App\Policies\UserPolicy::getUserThumbnail
     * @covers App\Http\Requests\User\GetUserThumbnailRequest
     * 
     * @depends test_update_user_thumbnail
     * 
     * @param User $user - The previously created user
     * @return void
     */
    public function test_get_user_thumbnail (User $user)
    {
        // Authenticate as user
        Passport::actingAs($user);
        // Test get thumbnail
        $response = $this->get('/api/users/' . $user->id . '/thumbnail');
        $response->assertStatus(200);
        // Then, delete file
        Storage::disk('thumbnails')->delete($user->thumbnail);
    }

    /**
     * Test check email availability
     * 
     * @author Hervé Perchec
     * @group User
     * @covers App\Http\Controllers\UserController::checkEmail()
     * @covers App\Http\Requests\User\CheckEmailRequest
     * 
     * @return void
     */
    public function test_check_email ()
    {
        // Valid request
        $response = $this->post('/api/users/check-email', [
            'email' => 'not_used_email@example.com'
        ]);
        $response->assertStatus(200);

        // Invalid request
        $response = $this->post('/api/users/check-email', [
            'email' => 'admin@app.com' // Already used email
        ]);
        $response->assertStatus(409);
    }
}
