<?php

namespace App\Http\Requests\Auth;

use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

use App\Http\Requests\BaseRequest;

/**
 * @OA\Schema(schema="Requests.Auth.GetCurrentUser")
 */
class GetCurrentUserRequest extends BaseRequest
{

    /**
     * Determine if the user is authorized to make this request
     * @return bool
     */
    public function authorize()
    {
        // Authorize -> App\Policies\AuthPolicy::getCurrentUser
        Gate::authorize('get-current-user');
        return true;
    }

    /**
     * Get the validation rules that apply to the request
     * @return array
     */
    public function rules()
    {
        return [];
    }

    /**
     * Get the error messages for the defined validation rules
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
