<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Auth\Access\AuthorizationException;

use App\Models\Error;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $exception)
    {
        // Overwrite behavior for ModelNotFoundException (findOrFail/firstOrFail/etc... see Eloquent Model class methods)
        if ($exception instanceof ModelNotFoundException) {
            return errorResponse(404, Error::find('e0015'));
        }

        // Overwrite behavior for AuthorizationException for policies (see also Illuminate\Auth\Access\Gate & Illuminate\Auth\Access\Response)
        // Responses are in JSON format now
        if ($exception instanceof AuthorizationException) {
            // Get error
            $error = $exception->response()->message();
            // Get response code
            $code = $exception->response()->code() ?: 403;
            // Check if instance of Error
            if ($error instanceof Error) {
                return errorResponse($code, $error);
            } else {
                return response($error, $code);
            }
        }

        return parent::render($request, $exception);
    }

}
