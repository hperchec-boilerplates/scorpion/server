<?php

namespace App\Services;

// OVERRIDE Laravel core class Illuminate\Validation\Validator
// see comments below

class Validator extends \Illuminate\Validation\Validator {

    /**
     * Add a failed rule and error message to the collection.
     *
     * @param  string  $attribute
     * @param  string  $rule
     * @param  array  $parameters
     * @return void
     */
    public function addFailure($attribute, $rule, $parameters = [])
    {
        if (! $this->messages) {
            $this->passes();
        }

        $attribute = str_replace(
            [$this->dotPlaceholder, '__asterisk__'],
            ['.', '*'],
            $attribute
        );

        if (in_array($rule, $this->excludeRules)) {
            return $this->excludeAttribute($attribute);
        }

        // ! OVERRIDE !
        // ------------
        // We override this method (addFailure) of Validator
        // to custom validation error format of the response (see below)
        // 
        // Example :
        //
        // (Default)
        // ```json
        // "email": [
        //     0: "The email has already been taken"
        // ]
        // ```
        //
        // (TO)
        // ```json
        // "email": {
        //     "rule:Unique": "The email has already been taken"
        // }
        // ```
        // 
        // ------------

        // $this->messages->add($attribute, $this->makeReplacements(
        //     $this->getMessage($attribute, $rule), $attribute, $rule, $parameters
        // ));

        // ------------

        $this->messages->add($attribute, [ 
            "rule:$rule" => $this->makeReplacements(
                $this->getMessage($attribute, $rule), $attribute, $rule, $parameters
            )
        ]);

        // END OVERRIDE
        // ------------

        $this->failedRules[$attribute][$rule] = $parameters;
    }

}