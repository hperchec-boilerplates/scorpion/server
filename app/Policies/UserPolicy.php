<?php

namespace App\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use App\Models\User;
use App\Models\Error;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the current user can update data
     * @param  \App\Models\User  $me - The authenticated user
     * @param  \App\Models\User  $user - The target user
     * @return mixed
     */
    public function update (User $me, User $user)
    {
        // Authenticated user can only update its OWN data
        if ($me->id !== $user->id) {
            return $this->deny(Error::find('e0012'));
        }
        // Everything is ok...
        return true;
    }

    /**
     * Determine whether the current user can update thumbnail
     * @param  \App\Models\User  $me - The authenticated user
     * @param  \App\Models\User  $user - The target user
     * @return mixed
     */
    public function updateThumbnail (User $me, User $user)
    {
        // Authenticated user can only update its OWN thumbnail
        if ($me->id !== $user->id) {
            return $this->deny(Error::find('e0012'));
        }
        // Everything is ok...
        return true;
    }

    /**
     * Determine whether the current user can get user thumbnail
     * @param  \App\Models\User  $user
     * @return mixed
     */
    public function getUserThumbnail (User $user)
    {
        // Everything is ok...
        return true;
    }

    /**
     * Determine whether the current user can get user thumbnail
     * @param  \App\Models\User  $me - The authenticated user
     * @param  \App\Models\User  $user - The target user
     * @return mixed
     */
    public function resendEmailVerificationLink (User $me, User $user)
    {
        // Target user must be authenticated user
        if ($me->id !== $user->id) {
            return $this->deny(Error::find('e0012'));
        }
        // Everything is ok...
        return true;
    }

}
