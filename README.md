# App (Server)

[![app](https://img.shields.io/static/v1?label=&message=App&color=362F2D&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAACWklEQVQ4T4WSTUhUURTH753PN2MTRZAtAinKdiGt2ojbCAIrmEghU4i+aMApyU3ja2aYmaaNOBLRQl0UUUESgSESDhTkJiRKTFv0gVEkBco0737MPaf7RuerUu/q8e45v3v+//+hpOoszya2+Ti5TySZcS1u6qWHQ7z6/n/ftPSTzSd3OyUdIxz2EQaEcJyGnGrzHjHfrwcpAwqzybsoSftKM8wgUxOEkS5lFZp8wfjHtSAVwLtEBwoyYgOQawiDTuDwkwhsMIKxwQ0BOGVuLjjcP/TrXrSnYAgoVMrz1tlHTbOwIcAukK/i87p5r262ZRAbRBlmJYe2urOJb+uaWAS8iH1HhvV2sy0FGC5QrqaIBQe1nNO+y+nnf0PKHtgXIhvjutFT9KEkg5NG+lueQIlRtFTUIIG4lqRfWDllAI7frJNOKwcWXHJwyKyOn3crdwP/tbSFKNeHIpTjhMFkO81kFmsBk+ZOyelrz6G+evEgcpEwdZwKfOk+k4jYhez6lWW0IGBPRwV5Ytzqb60B5J6Z+z2KvEEBQe+x6KNqrcwMN6Kic9qLojc62mHfnYGuGoAcu9aC0pnVC/QVODb7TlWWJ2f27HBx+KwlfNE+7NEmX/UPD6ZrAPyp2UoFjK6aJwhXEe+F1I3SJFZPdwvmIUwENFPpPOAb6f9UAxCPI53a6aHiiAxQ74LwgGMX7a7knz8fmlachANDA5P/pCAemk0gCuOU43Y9ogCuEsaSP6kjE6Xi/LnQUf/tgdFqf2r2AO/1buU5R4oyOKnjcusktKmODiOenltrlf8Awt9jILDjUAQAAAAASUVORK5CYII=)](https://gitlab.com/hperchec/boilerplates/scorpion/root)
[![app-server](https://img.shields.io/static/v1?labelColor=362F2D&label=server&message=v1.0.0&color=362F2D&logo=data:image/ico;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAD///8S////I////yP///8j////I////yP///8j////I////yP///8j////I////yP///8j////I////yP///8S////v////9/////f////3////9/////f////3////9/////f////3////9/////f////3////9/////f////v//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////+////j////4////+P////j/////7//////////////////////////////////////////////////////////v///4////+P////j////4/////+///////////////////////////////////////////////////////////////////////////////////////////////A////4f///+D////g////4P///+D////g////4P///+D////g////4P///+D////g////4P///+H////A////D////x3///8d////Hf///x3///8d////Hf///x3///8d////Hf///x3///8d////Hf///x3///8d////D////w////8d////Hf///x3///8d////Hf///x3///8d////Hf///x3///8d////Hf///x3///8d////Hf///w/////A////4f///+D////g////4P///+D////g////4P///+D////g////4P///+D////g////4P///+H////A//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////7///+P////j////4////+P/////v/////////////////////////////////////////////////////////+////j////4////+P////j/////7//////////////////////////////////////////////////////////////////////////////////////////////7/////f////3////9/////f////3////9/////f////3////9/////f////3////9/////f////3////7////8S////I////yP///8j////I////yP///8j////I////yP///8j////I////yP///8j////I////yP///8SAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA==)](https://gitlab.com/hperchec/boilerplates/scorpion/server)
[![Docker](https://shields.io/static/v1?logo=docker&logoColor=white&label=&labelColor=2496ED&message=Docker&color=2496ED)](https://docker.com/)
[![Laravel](https://shields.io/static/v1?logo=laravel&logoColor=white&label=&labelColor=FB7470&message=Laravel&color=FB7470)](https://laravel.com/)
[![Discord](https://img.shields.io/discord/972997305739395092?label=Discord&labelColor=5562EA&logo=discord&logoColor=white&color=2F3035)](https://discord.gg/AWTgVAVKaR)
[![pipeline status](https://gitlab.com/hperchec-boilerplates/scorpion/server/badges/main/pipeline.svg)](https://gitlab.com/hperchec/boilerplates/scorpion/server/commits/main)

[![author](https://img.shields.io/static/v1?label=&message=Author:&color=black)]()
[![herve-perchec](http://herve-perchec.com/badge.svg)](http://herve-perchec.com/)

**Table of contents**:

[[_TOC_]]

## Requirements

- PHP 7.3.21 [https://www.php.net/manual/fr/](https://www.php.net/manual/fr/)
- Composer 2.0.13 [https://getcomposer.org/download/](https://getcomposer.org/download/)

## Get started

> **IMPORTANT**: Please refer to the parent repository: [app](https://gitlab.com/hperchec/boilerplates/scorpion/root.git).

If you want to clone this repository only:

```bash
git clone https://gitlab.com/hperchec/boilerplates/scorpion/server.git
```

### Environment

```bash
First, copy/paste `.env.dev.example` and rename it to `.env`
```

Let's have a look to the .env file most important variables you should configure (expand):

<details>

<summary markdown="span">Environment variables</summary>

|Value                       |Description                                                                                             |
|-                           |-                                                                                                       |
|APP_NAME                    |Application name                                                                                        |
|APP_ENV                     |Environment (*local*, *production*, or *testing*)                                                       |
|APP_KEY                     |Application key (generated via `php artisan key:generate`)                                              |
|APP_TIMEZONE                |Application time zone                                                                                   |
|APP_URL                     |Application production URL                                                                              |
|APP_DEV_URL                 |Application development URL                                                                             |
|APP_UI_DEV_URL              |Application development URL for UI interface (Note the '#' that means Vue.js + vue-router are used)     |
|DB_CONNECTION               |Define what database connection must be used (see also *config/database.php* file)                      |
|DB_HOST                     |Define the database host                                                                                |
|DB_PORT                     |Define the database port                                                                                |
|DB_DATABASE                 |Define what database must be used                                                                       |
|DB_USERNAME                 |Define the database username                                                                            |
|DB_PASSWORD                 |Define the database user password                                                                       |
|MAIL_MAILER                 |Define the type of mailer that must be used (ex: *smtp*)                                                |
|MAIL_HOST                   |Define the mailer host (ex: *smtp.gmail.com*)                                                           |
|MAIL_PORT                   |Define the mailer port (ex: *465*)                                                                      |
|MAIL_USERNAME               |The mail address to connect to mail server                                                              |
|MAIL_PASSWORD               |The password for mail user                                                                              |
|MAIL_ENCRYPTION             |Define the type of encryption (ex: *ssl*)                                                               |
|MAIL_FROM_ADDRESS           |Define the mail address used to send emails                                                             |
|MAIL_FROM_NAME              |Define the name to display in emails received from *MAIL_FROM_ADDRESS*                                  |
|MAIL_ALWAYS_TO              |Define a specific mail address that will be always used to receive emails (useful for testing, only available in *local* mode, see also: *app/Providers/AppServiceProvider -> boot()*) |
|L5_SWAGGER_GENERATE_ALWAYS  |Define if Swagger documentation must be regenerated at each request                                     |

</details>

### Dependencies

```bash
# Install dependencies
composer install
```

Then, generate app key:

```bash
# Generate app key
php artisan key:generate
```

Finally, generate passport oauth private key:

```bash
php artisan passport:keys
```

### Public folder

```bash
# Link storage (symbolic link from /storage/app/public to /public/storage)
php artisan storage:link
```

## Let's run

Start the server:

```bash
# Run server
php artisan serve
# In the docker container, run instead:
php artisan serve --host=0.0.0.0 --port=8000
```

## Swagger documentation

> Swagger documentation is regenerated at each request. To configure it, see `.env` file -> `L5_SWAGGER_GENERATE_ALWAYS`

```bash
# (Re)Generate swagger documentation
php artisan l5-swagger:generate
# Check at http://localhost:8000/api/documentation
```

## Errors translations

Export errors translations from database to json files for 'front' app

It will generate:

- /storage/app/errors/en.json
- /storage/app/errors/fr.json

```bash
# Export errors in .json files
php artisan export:errors
```

Once errors files are generated, copy/paste files in 'ui' directory:

- `/storage/app/errors/en.json` to `ui/src/i18n/errors/en.json`
- `/storage/app/errors/fr.json` to `ui/src/i18n/errors/fr.json`

## Tests

> Check [Laravel documentation](https://laravel.com/docs/8.x/testing)

First, create the `.env.testing`

```bash
copy .env.testing.example .env.testing
```

If you use another database for tests (for example: **app_test**), don't forget to specify the name in the `.env.testing` file.
{: .alert .alert-danger}

Generate APP_KEY in the `.env.testing` file:

```bash
php artisan key:generate --env=testing
```

```bash
# Run tests
php artisan tests:run
```

> Note that `xdebug` php extension must be installed and configured to make log and coverage report generating possible
>  
> PHPUnit outputs will be in the test/report folder:
>  
> - coverage.txt
> - report.xml

## Docker

### Build the image

```bash
docker build -t php-7.3.21 .
```

> Note that `php-7.3.21` will be the name of the image. You can modify it as you want.

### Run the container

This command will run the PHP container named `php` based on the `php-7.3.21` image previously created.

Note that the **container port** `8000` will be related to the **local port** `8000`.

```bash
docker run -d -p 8000:8000 --name php php-7.3.21
```

### Using GitLab

> Read documentation: [https://docs.gitlab.com/ee/ci/examples/laravel_with_gitlab_and_envoy/#setting-up-gitlab-container-registry](https://docs.gitlab.com/ee/ci/examples/laravel_with_gitlab_and_envoy/#setting-up-gitlab-container-registry)

Login to gitlab registry:

```bash
docker login registry.gitlab.com
```

Build and push image to gitlab registry:

```bash
# Build the image (using 'prod' stage, see Dockerfile)
docker build --target prod -t registry.gitlab.com/hperchec/boilerplates/scorpion/server .
# Push the image to the container registry
docker push registry.gitlab.com/hperchec/boilerplates/scorpion/server
```

----

Made with ❤ by [**Hervé Perchec**](http://herve-perchec.com/)