<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Error;

class ErrorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Error::insert([

            // e0000 > e0009 : Global system errors
            [
                "error_id" => "e0000",
                "fr_message" => "Erreur inconnue",
                "en_message" => "Unknown error"
            ],

            // e0010 > e0019 : Network requests errors
            [
                "error_id"   => "e0010",
                "fr_message" => "Une erreur est survenue lors du traitement de la requête. Veuillez ré-essayer.",
                "en_message" => "An error has occured during the request. Please try again."
            ],
            [
                "error_id"   => "e0010-400",
                "fr_message" => "Requête invalide",
                "en_message" => "Bad Request"
            ],
            [
                "error_id"   => "e0010-401",
                "fr_message" => "Authentification requise",
                "en_message" => "Unauthenticated"
            ],
            [
                "error_id"   => "e0010-402",
                "fr_message" => "Paiement requis",
                "en_message" => "Payment Required"
            ],
            [
                "error_id"   => "e0010-403",
                "fr_message" => "Non autorisé",
                "en_message" => "Forbidden"
            ],
            [
                "error_id"   => "e0010-404",
                "fr_message" => "Introuvable",
                "en_message" => "Not found"
            ],
            [
                "error_id"   => "e0010-405",
                "fr_message" => "Méthode non autorisée",
                "en_message" => "Method Not Allowed"
            ],
            [
                "error_id"   => "e0010-409",
                "fr_message" => "Conflit",
                "en_message" => "Conflict"
            ],
            [
                "error_id"   => "e0010-422",
                "fr_message" => "Entité fournie invalide ou incomplète",
                "en_message" => "Unprocessable entity"
            ],
            [
                "error_id"   => "e0010-500",
                "fr_message" => "Erreur interne du serveur",
                "en_message" => "Internal Server Error"
            ],
            [
                "error_id"   => "e0010-501",
                "fr_message" => "Non implémenté",
                "en_message" => "Not Implemented"
            ],
            [
                "error_id"   => "e0010-502",
                "fr_message" => "Mauvaise passerelle / Erreur proxy",
                "en_message" => "Bad Gateway / Proxy Error"
            ],
            [
                "error_id"   => "e0010-503",
                "fr_message" => "Service non disponible",
                "en_message" => "Service Unavailable"
            ],
            [
                "error_id"   => "e0010-504",
                "fr_message" => "Temps d'attente de réponse dépassé",
                "en_message" => "Gateway Time-out"
            ],
            [
                "error_id"   => "e0011",
                "fr_message" => "Une erreur est survenue lors du chargement des données. Veuillez ré-essayer.",
                "en_message" => "An error has occured during the data loading. Please try again."
            ],
            [
                "error_id"   => "e0012",
                "fr_message" => "Vous n'êtes pas autorisé à effectuer cette action.",
                "en_message" => "You don't have permission to do this action"
            ],
            [
                "error_id"   => "e0013",
                "fr_message" => "Vous n'êtes pas autorisé à effectuer l'action suivante: {action}",
                "en_message" => "You don't have permission to do the following action: {action}"
            ],
            [
                "error_id"   => "e0015",
                "fr_message" => "Entité introuvable",
                "en_message" => "Not found entity"
            ],

            // e0020 > e0029 : Authentication/authorization errors
            [
                "error_id"   => "e0020",
                "fr_message" => "Identifiants incorrects",
                "en_message" => "Bad credentials"
            ],
            [
                "error_id"   => "e0021",
                "fr_message" => "Session invalide ou expirée. Veuillez vous reconnecter.",
                "en_message" => "Invalid or expired session. Please try to log again."
            ],
            [
                "error_id"   => "e0022",
                "fr_message" => "Le mot de passe saisi est incorrect",
                "en_message" => "Invalid password given"
            ],
            [
                "error_id"   => "e0023",
                "fr_message" => "Vous devez être le propriétaire de la ressource pour effectuer cette action.",
                "en_message" => "You must be the owner of the resource to do this action."
            ],

            // e0030 > e0039 : Sign up errors
            [
                "error_id"   => "e0030",
                "fr_message" => "Le mot de passe de respecte pas les règles de sécurité. Veuillez ré-essayer.",
                "en_message" => "Password does not match security policy. Try again."
            ],
            [
                "error_id"   => "e0031",
                "fr_message" => "L'email que vous avez saisi est déjà utilisée.",
                "en_message" => "Email has already been taken."
            ],
            [
                "error_id"   => "e0032",
                "fr_message" => "L'email saisi ne respecte pas le format requis.",
                "en_message" => "Email doesn't match required format"
            ],

            // e0040 > e0049 : Other user related errors
            [
                "error_id"   => "e0040",
                "fr_message" => "Le lien de vérification d'adresse email n'est plus valide. Veuillez ré-essayer.",
                "en_message" => "The email verification link is not valid. Please try again."
            ],
            [
                "error_id"   => "e0041",
                "fr_message" => "Le mot de pâsse....",
                "en_message" => "Password..."
            ],

            // e0050 > e0059 : Global form errors
            [
                "error_id"   => "e0050",
                "fr_message" => "Un champ du formulaire n'est pas valide",
                "en_message" => "A form field is invalid"
            ],
            [
                "error_id"   => "e0051",
                "fr_message" => "Ce champ n'est pas valide",
                "en_message" => "This field is invalid"
            ],
            [
                "error_id"   => "e0052",
                "fr_message" => "Le champ {field} n'est pas valide",
                "en_message" => "The field {field} is invalid"
            ]

            // e0100 > ...? : 'Business' errors

        ]);

    }
}
